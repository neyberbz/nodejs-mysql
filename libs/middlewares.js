module.exports = app =>{

    const morgan = require('morgan');
    const bodyParser = require('body-parser');

    app.set('json spaces', 4);
    app.set('port', process.env.PORT || 3333);

    app.use(morgan('dev'));
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());

    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });
}