const mysql = require('mysql');
const moment = require('moment');
const nodemailer = require('nodemailer');

connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: '7tual'
})

let authModel = {};

// 0: Autenticacion fallida. Email erroneo.
// 1: Autenticacion correcta.
// 2: Autenticacion fallida. Contrasena erronea.

authModel.loginAuth = (userData, callback) => {

    var dateNow = moment().toISOString().replace(/T/, ' ').replace(/\..+/, '');

    if(connection){
        var sqlExists =  `SELECT * FROM 7t_usuarios WHERE email = ${connection.escape(userData.email)}`;
        connection.query(sqlExists, (err, results) => {
            if (results.length > 0) {
                if(results[0].contrasenya != userData.contrasenya){
                    callback(null, {
                        "msg": 2
                    });
                }else{
                    const sql = `
                        UPDATE 7t_usuarios SET 
                        ultimo_logeo = '${dateNow}'
                        WHERE usuario_ID = ${results[0].usuario_ID}
                    `;
                    connection.query(sql, (err, result) => {
                        if (err) {
                            throw err;
                        } else {
                            callback(null, {
                                "msg": 1
                            });
                        }
                    });
                }
            }else{
                callback(null, {
                    "msg": 0
                });
            }
        });
    }
};

authModel.verifyCuenta = (userData, callback) => {
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: 'smtp.ethereal.email',
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: account.user, // generated ethereal user
            pass: account.pass  // generated ethereal password
        }
    });

    // setup email data with unicode symbols
    let mailOptions = {
        from: '"Fred Foo 👻" <foo@blurdybloop.com>', // sender address
        to: 'enjey11@gmail.com', // list of receivers
        subject: 'Hello ✔', // Subject line
        text: 'Hello world?', // plain text body
        html: '<b>Hello world?</b>' // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);
        // Preview only available when sending through an Ethereal account
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@blurdybloop.com>
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    });
};

module.exports = authModel;