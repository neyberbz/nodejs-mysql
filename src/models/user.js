const mysql = require('mysql');
const moment = require('moment');

connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: '7tual'
})

let userModel = {};

userModel.getUser = (callback) => {
    if(connection){
        connection.query('SELECT * FROM 7t_usuarios',
            (err, rows) => {
                if(err){
                    throw err
                }else{
                    callback(null, rows);
                }
            }
        )
    }
};

userModel.insertUser = (userData, callback) => {
    if(connection){
        connection.query('INSERT INTO 7t_usuarios SET ?', userData,
            (err, result) => {
                if(err){
                    throw err;
                }else{
                    callback(null, {
                        'insertId': result.insertId
                    });
                }
            }
        )
    }
};

userModel.updateUser = (userData, callback) => {

    var dateNow = moment().toISOString().replace(/T/, ' ').replace(/\..+/, '');

    if (connection) {
        const sql = `
            UPDATE 7t_usuarios SET 
            nombres = ${connection.escape(userData.nombres)}, 
            email = ${connection.escape(userData.email)}, 
            contrasenya = ${connection.escape(userData.contrasenya)},
            modified_at = '${dateNow}'
            WHERE usuario_ID = ${userData.usuario_ID}
        `;

        connection.query(sql, (err, result) => {
            if (err) {
                throw err;
            } else {
                callback(null, {
                    "msg": "success"
                })
            }
        });
    }
};

userModel.deleteUser = (id, callback) => {
    if (connection) {
        var sqlExists = `SELECT * FROM 7t_usuarios WHERE usuario_ID = ${connection.escape(id)}`;
        connection.query(sqlExists, (err, row) => {
            if (row) {
                var sql = `DELETE FROM 7t_usuarios WHERE usuario_ID=` + connection.escape(id);
                connection.query(sql, (err, result) => {
                    if (err) {
                        throw err;
                    } else {
                        callback(null, {
                            "msg": "deleted"
                        });
                    }
                });
            } else {
                callback(null, {
                    "msg": "not Exists"
                });
            }
        });
    }
};

module.exports = userModel;