const mysql = require('mysql')

connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: '7tual'
})

let ubigeoModel = {};

ubigeoModel.getUbigeo = (callback) => {
    if(connection){
        connection.query('SELECT * FROM 7t_ubigeos WHERE char_length(ubigeo_ID) = 2',
            (err, rows) => {
                if(err){
                    throw err
                }else{
                    callback(null, rows);
                }
            }
        )
    }
};

module.exports = ubigeoModel;