const express = require('express')
const consign = require('consign')
const morgan = require('morgan')
const bodyParser = require('body-parser')

const app = express();

// settings
app.set('port', process.env.PORT || 3333 );

// middlewares
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// routes
require('./routes/userRoutes')(app);
require('./routes/authRoutes')(app);
require('./routes/ubigeoRoutes')(app);
require('./routes/apiRoutes')(app);

// static files

app.listen(app.get('port'), () => {
    console.log('server on port 3333!');
});

// consign()
//     .include('libs/middlewares.js')
//     .then('libs/routes.js')
//     .include('libs/boots.js')
//     .into(app);