const Ubigeo = require('../models/ubigeo')

module.exports = function (app){

    app.get('/api/ubigeos', (req, res) => {
        Ubigeo.getUbigeo((err, data) => {
            res.status(200).json(data);
        });
    });

};