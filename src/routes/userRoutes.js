const User = require('../models/user');

var crypto = require('crypto'),
    algorithm = 'aes256',
    password = 'd6F3Efeq';

function encrypt(text){
    var cipher = crypto.createCipher(algorithm,password)
    var crypted = cipher.update(text,'utf8','hex')
    crypted += cipher.final('hex');
    return crypted;
}

function decrypt(text){
    var decipher = crypto.createDecipher(algorithm,password)
    var dec = decipher.update(text,'hex','utf8')
    dec += decipher.final('utf8');
    return dec;
}

module.exports = function (app){

    app.get('/api/users', (req, res) => {
        User.getUser((err, data) => {
            res.status(200).json(data);
        });
    });

    app.post('/api/users', (req, res) => {
        var userData = {
            usuario_ID: null,
            nombres: req.body.nombres,
            email: req.body.email,
            contrasenya: req.body.contrasenya,
            ultimo_logeo: req.body.ultimo_logeo,
            created_at: req.body.created_at,
            modified_at: req.body.modified_at
        };
        User.insertUser(userData, (err, data) => {
            if(data && data.insertId){
                console.log(data);
                res.status(200).json({
                    success: true,
                    msg: "Nuevo usuario insertado",
                    data: data
                });
            }else{
                res.status(500).json({
                    success: false,
                    msg: "Error"
                })
            }
        })
    });

    app.put('/api/users/:id', (req, res) => {
        var userData = {
            usuario_ID: req.params.id,
            nombres: req.body.nombres,
            email: req.body.email,
            contrasenya: encrypt(req.body.contrasenya)
        };

        User.updateUser(userData, function (err, data) {
            if (data && data.msg) {
                console.log(encrypt("hello world"));
                res.status(200).json({
                    data
                });
            } else {
                res.status(500).json({
                    success: false,
                    msg: 'Error'
                });
            }
        });
    });

    app.delete('/api/users/:id', (req, res) => {
        var id = req.params.id;
        User.deleteUser(id, (err, data) => {
            if (data && data.msg === 'deleted' || data.msg == 'not Exists') {
                res.status(200).json({
                    success: 'true',
                    data
                });
            } else {
                res.status(500).json({
                    msg: 'Error'
                });
            }
        });
    });

};