const Auth = require('../models/auth');
const config = require('../../libs/configToken');
const crypto = require('crypto');
const nodemailer = require('nodemailer');

module.exports = function (app){

    app.post('/api/auth', (req, res) => {
        const encrypted = crypto.createHmac('sha1', config.secretPwd)
                      .update(req.body.contrasenya)
                      .digest('base64');

        var userData = {
            email: req.body.email,
            contrasenya: encrypted
        };

        Auth.loginAuth(userData, (err, data) => {
            if(data){
                res.status(200).json({
                    success: true,
                    msg: data.msg,
                });
            }else{
                res.status(500).json({
                    success: false,
                    msg: "Error"
                })
            }
        });
    });

    app.get('/api/testAcount', (req, res) => {
        
        let transporter = nodemailer.createTransport({
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: 'dp45jh5lserybdrz@ethereal.email',
                pass: '6Au8V6QJvEwnrxr2gt'
            }
        });
    
        // setup email data with unicode symbols
        let mailOptions = {
            from: '"Fred Foo 👻" <dp45jh5lserybdrz@ethereal.email>', // sender address
            to: 'enjey11@gmail.com', // list of receivers
            subject: 'Hello ✔', // Subject line
            text: 'Hello world?', // plain text body
            html: '<b>Hello world?</b>' // html body
        };
    
        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
    
            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@blurdybloop.com>
            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        });
    });


};